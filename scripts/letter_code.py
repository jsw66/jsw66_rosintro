#!/usr/bin/env python

# Author: Jerry Wang
# Adapted from code by: Acorn Pooley, Mike Lautman

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class letter_code(object):

    def __init__(self):
        super(letter_code, self).__init__()

        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object.
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        # We can get the name of the reference frame for this robot:
        planning_frame = move_group.get_planning_frame()
        # We can also print the name of the end-effector link for this group:
        eef_link = move_group.get_end_effector_link()
        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()

        # Assigning misc variables
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

    def go_to_joint_state(self):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        # The below values joint values were found by determining the ideal starting point manually
        joint_goal[0] = -3.227
        joint_goal[1] = -1.955
        joint_goal[2] = -1.123
        joint_goal[3] = -3.206
        joint_goal[4] = 3.056
        joint_goal[5] = -1.460
        print("Returning to Default State")
        move_group.go(joint_goal, wait=True)

        # Calling ``stop()`` ensures that there is no residual movement
        move_group.stop()

    def diagonal_right(self, wpose, scale, direction, size):
        """
        # This function is used to move diagonal in the right direction, either up or down as indicated by the "direction" parameter
        """
        waypoints = []
        # Multiplier determines vertical z-motion as up or down
        multiplier = 1;
        if (direction == -1):
            multiplier = -1;
        for i in range(size):
            wpose.position.y += scale * 0.03 # Move slightly right (y)
            waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
            wpose.position.z += multiplier * scale * 0.05 # Move slightly vertically (z)
            waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
        return waypoints

    def diagonal_left(self, wpose, scale, direction, size):
        """
        # This function is used to move diagonal in the left direction, either up or down as indicated by the "direction" parameter
        """
        waypoints = []
        # Multiplier determines vertical z-motion as up or down
        multiplier = 1;
        if (direction == -1):
            multiplier = -1;
        for i in range(size):
            wpose.position.y -= scale * 0.03 # Move slightly left (y)
            waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
            wpose.position.z += multiplier * scale * 0.05 # Move slightly vertically (z)
            waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
        return waypoints

    def write_J(self, scale=1):
        move_group = self.move_group
        waypoints = []
        wpose = move_group.get_current_pose().pose
        """
        # OBSERVE: The letter to write is a J
        """
        wpose.position.z -= scale * 0.2  # First move down (z)
        waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
        waypoints.extend(self.diagonal_left(wpose, scale,-1,2)) # A small left diagonal down
        waypoints.extend(self.diagonal_left(wpose, scale,1,2)) # A small left diagonal up
        # This above creates a 'J'

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def write_S(self, scale=1):
        move_group = self.move_group
        waypoints = []
        wpose = move_group.get_current_pose().pose
        """
        # OBSERVE: The letter to write is a S
        """
        wpose.position.y -= scale * 0.15 # Move left (y)
        waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
        waypoints.extend(self.diagonal_right(wpose, scale,-1,4)) # A right diagonal down
        wpose.position.y -= scale * 0.15 # Move left (y)
        waypoints.append(copy.deepcopy(wpose)) # Append to waypoint vector every time
        # The above draws an 'S' (it will look somewhat like a backwards 'Z')

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def write_W(self, scale=1):
        move_group = self.move_group
        waypoints = []
        wpose = move_group.get_current_pose().pose
        """
        # OBSERVE: The letter to write is a W
        """
        waypoints.extend(self.diagonal_right(wpose, scale,-1,5)) # A right diagonal down
        waypoints.extend(self.diagonal_right(wpose, scale,1,3)) # A smaller right diagonal up
        waypoints.extend(self.diagonal_right(wpose, scale,-1,3)) # A smaller right diagonal down
        waypoints.extend(self.diagonal_right(wpose, scale,1,5)) # A right diagonal up
        # The above four diagonals create a 'W'

        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold

        # Note: We are just planning, not asking move_group to actually move the robot yet:
        return plan, fraction

    def display_trajectory(self, plan):
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        ## We populate the trajectory_start with our current robot state to copy over
        ## any AttachedCollisionObjects and add our plan to the trajectory.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        display_trajectory_publisher.publish(display_trajectory)

    def execute_plan(self, plan):
        # Execute the movement plan
         self.move_group.execute(plan, wait=True)

def main():
# Here we run the steps to compelte the program on the simulation
    try:
        print("")
        print("----------------------------------------------------------")
        print("Write a Letter for the Midterm")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")
        print("")

        output_motion = letter_code()

        output_motion.go_to_joint_state()
        # This will move the robot
        input("============ Press `Enter` to write your initials ...")
        # First write the J
        print("Writing a J...")
        cartesian_plan, fraction = output_motion.write_J()
        output_motion.execute_plan(cartesian_plan)
        # Return to our defined origin
        output_motion.go_to_joint_state()

        # Second write the S
        print("Writing an S...")
        cartesian_plan, fraction = output_motion.write_S()
        output_motion.execute_plan(cartesian_plan)
        # Return to our defined origin
        output_motion.go_to_joint_state()

        # Third write the W
        print("Writing a W...")
        cartesian_plan, fraction = output_motion.write_W()
        output_motion.execute_plan(cartesian_plan)
        # Return to our defined origin and terminate here
        output_motion.go_to_joint_state()

        print("============ Initials complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == "__main__":
    main()
